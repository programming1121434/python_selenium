from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.by import By
import time, requests
from base64 import b64encode
import os
from dotenv import load_dotenv

def capture_screenshot(url, output_path, delay=15):
    chrome_options = Options()
    chrome_options.add_argument('--headless')
    chrome_options.add_argument('--disable-gpu')
    chrome_options.add_argument('--disable-software-rasterizer')
    chrome_options.add_argument('--no-sandbox')
    chrome_options.add_argument('--disable-dev-shm-usage')
    chrome_options.add_argument('--window-size=1920x1080')

    driver = webdriver.Chrome(options=chrome_options)

    try:
        driver.get(url)

        login_button = driver.find_element(By.CLASS_NAME, "css-1gnviw9-button")
        login_button.click()

        driver.implicitly_wait(5)
        username_input = driver.find_element(By.NAME, "login")
        submit_button = driver.find_element(By.ID, 'submit')
        username_input.send_keys("")
        submit_button.click()

        password_input = driver.find_element(By.NAME, "password")
        password_input.send_keys("")
        sign_in_button = driver.find_element(By.ID, 'submit')
        sign_in_button.click()

        driver.implicitly_wait(10)
        time.sleep(delay)

        driver.save_screenshot(output_path)
        print(f"Screenshot captured and saved to: {output_path}")

    except Exception as e:
        print(f"An error occurred: {str(e)}")

def send_image_to_line_notify(token, image):
    try:
        token = token
        picture = image
        payload = {'message' : 'ETAX Azure SQL CPU Usage Last 6 hour'}
        r = requests.post('https://notify-api.line.me/api/notify'
                        , headers={'Authorization' : 'Bearer {}'.format(token)}
                        , params = payload
                        , files = {'imageFile': open(picture, 'rb')})
    except Exception as e:
        print(f"An error occurred: {str(e)}")
    
def send_image_to_teams(webhook_url, image_path):
    with open(image_path, "rb") as f:
        image_data = f.read()

    message = 'ETAX Azure SQL CPU Usage Last 6 hour.'

    adaptive_card = {
        "$schema": "http://adaptivecards.io/schemas/adaptive-card.json",
        "type": "AdaptiveCard",
        "version": "1.4",
        "body": [
            {
                "type": "TextBlock",
                "text": message,
                "weight": "bolder",
                "size": "medium"
            },
            {
                "type": "Image",
                "url": "data:image/png;base64," + str(b64encode(image_data).decode()),
                "size": "auto"
            }
        ]
    }

    payload = {
        "type": "message",
        "attachments": [
            {
                "contentType": "application/vnd.microsoft.card.adaptive",
                "content": adaptive_card
            }
        ]
    }

    response = requests.post(webhook_url, json=payload)
    print(response)

if __name__ == "__main__":
    load_dotenv
    grafana_url = os.getenv("GRAFANA_URL")
    line_token = os.getenv("LINE_NOTI_TOKEN")
    ms_teams_webhook = os.getenv("MS_TEAMS_WEBHOOK")


    website_url = ""
    output_file_path = "screenshot.png"
    
    capture_screenshot(website_url, output_file_path, delay=15)
    time.sleep(3)

    print("Send Image to Line Notify")
    token = line_token
    image = './screenshot.png'
    send_image_to_line_notify(token, image)
    time.sleep(3)

    print("Send Image to MS Teams Channel")
    webhook_url = ""
    image_path = './screenshot.png'
    send_image_to_teams(webhook_url, image_path)
    
