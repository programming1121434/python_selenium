# Introduction 
Python Selenium Capture Screen from Grafana and send to Line Notify and MS Teams channel.

- Running on Python version 3.11.5
- Install dependency package.
    ```sh
    pip install -r requirements.txt
    ```
- Add Grafana Username & Password to [main.py](./main.py)
    - Username to line [29](./main.py#29)
    - Password to line [33](./main.py#33)
- Create .env for store Line Notify token
    ```sh
    # .env
    LINE_NOTI_TOKEN="Line Notify Token"
    ```
- Change Grafana Dashboard Pannel URL
- Create MS Teams incoming webhook and replace in main.py line [116](./main.py#116)

- On Windows using Google Chrome and Install [Chrome driver](https://chromedriver.chromium.org/downloads)
- On Linux install [Google Chrome and Chrome driver](https://tecadmin.net/setup-selenium-chromedriver-on-ubuntu/)
- Running Python Script
    ```sh
    python main.py
    ```